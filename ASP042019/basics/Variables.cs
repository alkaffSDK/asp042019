﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asp032019
{

    /// <summary>
    /// Variable is a symbolic name (an identifier) paired with a storage location
    /// (identified by a memory address) which contains some known or unknown quantity of 
    /// information referred to as a value.

    /// Examples :
    /// int x = 10 ;
    /// Object y = new Object();

    /// Syntax :
    /// [modifiers] [access_specifier] <data_type> <variable_identifier> [=<expression>][, <variable_identifier> [=<expression>]]* ;
    /// Reading links :
    /// 
    /// https://www.tutorialspoint.com/csharp/csharp_data_types.htm
    /// </summary>
    class Variables
    {
        static void Main(string[] args)
        {
          
            bool cb = false;
            bool b = true;

            char ch = 'A';      // A
            Console.WriteLine(ch);

            ch = '\u0041';      // A, unicode in hexadecimal 
            Console.WriteLine(ch);

            // Integer types (byte, sbyte, short, ushort, int, uint, long , ulong)

            short Minsho = -32768, MaxSho = 32767;

            byte by = 255;
            sbyte sby = -50;
            long Lo = 152455885421474l;


            // Floating points  (float, double , decimal )

            float f = 1.7234567890123456789F;
            double d = 1.1234567890123456789;
            decimal m = 1.1234567890123456789M;

            Console.WriteLine("F:"+f);
            Console.WriteLine("D:" + d);
            Console.WriteLine("M:" + m);

            //int a = 10 + 25, m = 125 , e ;
            //int x = 10;
            //int c = 125487 ;


            bool boolean = false;
            uint ui = 10u;                       // [U]
            Console.WriteLine(10U);
            long i = 105555544421455L;           // [L]
            Console.WriteLine(10L);
            ulong ul = 10;                       // [LU|UL|U|L]

            uint dd = 10;

            int re = 010;
            Console.WriteLine(re);               // 10
            re = 0x10;                           // any number start with 0x is hexadecimal
            Console.WriteLine(re);               // 16


            ch = 'a';
            Console.WriteLine("A :" + ch);          // A: a
            Console.WriteLine(3 + 'a');             // 100
            Console.WriteLine("A :" + 3 + 'a');     // A :3a
            Console.WriteLine("A :" + (3 + "a"));   // A :3a
            Console.WriteLine("A :" + (3 + 'a'));   // A :100


            sby = 127;
            Console.WriteLine("sby:" + sby);
            sby++;
            Console.WriteLine("sby:" + sby);
            int to = 182247;
            byte bye =(byte) to;
            Console.WriteLine("bye:" + bye);
            Minsho =(short) to;


            int c = (int)15.325;

            Console.WriteLine("Minsho :"+ Minsho);
            // Type conversion
            short sh = (short)c;
            Console.WriteLine("sh: " + sh);
            int x = (int)f;
            x = Convert.ToInt32(f);
            Console.WriteLine("x: " + x);

            x = Convert.ToInt32(f);    // I need to convert f to int
           
            Console.WriteLine("X: "+x);
            bool flage = Convert.ToBoolean("false");
            Console.WriteLine(flage);      // false
            flage = Convert.ToBoolean(false);
            Console.WriteLine(flage);          // false
            Console.WriteLine(15);      // fals
            Console.WriteLine(flage);           // true

            bool.TryParse("false", out flage);
            Console.WriteLine(flage);

            x =  int.Parse("1254");
            Console.WriteLine("X: " + x);

            x = Convert.ToInt32("1254");
            Console.WriteLine("X: " + x);

            int.TryParse("1234t", out x);
            Console.WriteLine("X: " + x);
            //string s = x.ToString();
            //s = Convert.ToString(x);

            Console.ReadKey();
        }
    }
}

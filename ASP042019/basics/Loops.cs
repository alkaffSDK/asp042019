﻿using System;

namespace ASPNET022019
{
    class Loops
    {
        static void Main(string[] args)
        {
            // For loop
            //for (;;)
            //    ;


            int b, c;
            for (Console.WriteLine("A"), Console.WriteLine("A1"), b = 2, c = 2; ; Console.WriteLine("C"), Console.WriteLine("C1"), b = 2, c = 2)
                Console.WriteLine();

            //for loop 
            //int c = 0, d = 3;      // c = 9 
            // for ([value] ;[boolean_expression];[])   
            for (Console.WriteLine("A"); c++ < 10; Console.WriteLine("C"))
            {
                Console.WriteLine("Statement or block, c :" + c);
            }


            //// while 
            //int counter = 0;
            //while (counter < 10)
            //{
            //    Console.WriteLine(counter);
            //    counter++;
            //}


            short a = 10;
            while (a > 0)
            {
                Console.WriteLine(a);
                a++;
            }

            // IL , 32767 , 32777 , 32757, 32757 
            //Console.WriteLine("Final A :"+a );      // 32767   -32768


            do
            {
                
            }
            while (c < 10);



            Console.ReadKey();
        }
    }
}

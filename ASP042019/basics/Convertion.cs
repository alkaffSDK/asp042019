﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPNET022019
{
    class Convertion
    {


        static void Main(string[] args)
        {
            int a = 10;     // 4 bytes 
            long l = 10;    // 8 byte 

            a = (int) l;          // explicit conversion
            a = (int) 15.3;
            a = Convert.ToInt32(l);

      
            //  a = (int) "15";


            a = Convert.ToInt32("15");
            a = int.Parse("15");
            int.TryParse("15", out a);

            decimal d;
            d = Convert.ToDecimal("15.215");
            d = decimal.Parse("15.215");
            decimal.TryParse("15.215", out d);

            Console.Write("D:");
            decimal.TryParse(Console.ReadLine(), out d);
            Console.WriteLine("D value is :"+d);
            a = (int)d;
            Console.WriteLine("A value is :" + a);
            decimal b = d - a;
            Console.WriteLine("B value is :" + b);
            l = a;

            Console.ReadKey();
        }
    }
}

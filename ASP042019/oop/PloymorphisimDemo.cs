﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

namespace asp032019.oop
{

    enum Major
    {
        CS, CIS, MIS, SE, CE, NE
    }

    enum Degree
    {
        Bsc, Msc, Phd, FullProf, AssestProf
    }

    enum Title
    {
        Manager, Officer, DeskWorker, Supervisour
    }
    class Person
    {
        private static int _id = 1;
        public Person(string n, string ssn)
        {
            ID = _id++;
            Name = n;
            SSN = ssn;
        }
        public string Name { get; set; }
        public int ID { get; }
        public string SSN { get; private set; }
        public virtual string MyType()
        {
            return "Person";
        }


        public override string ToString()
        {
            return String.Format("{3,10} [ID:{0,5}, Name:{1,-25}, SSN:{2,6}]", ID, Name, SSN,"Person");
        }
    }

    class Student : Person
    {
        public int StudentNumber { get; private set; }

        public double AGP { get; set; }

        public Major Major { get; set; }
        public Student(string n, string ssn, int number, Major major) : base(n, ssn)
        {
            StudentNumber = number;
            AGP = 0;
            Major = major;
        }

        public override string MyType()
        {
            return "Student";
        }

        public override string ToString()
        {
            return String.Format("{5,10} [ID:{0,5}, Name:{1,-25}, SSN:{2,6}, Number:{3,6}, Major:{4,5}]",
                ID, Name, SSN,StudentNumber,Major, "Student");
        }


    }
    class Worker : Person
    {
        public Worker(string n, string ssn, double salary) : base(n, ssn)
        {
            Salary = salary;
        }

        public double Salary { get; set; }

        public override string MyType()
        {
            return "Worker";
        }
    }

    class Teacher : Worker
    {
        public Degree Degree { get; set; }
        public Teacher(string n, string ssn, double salry, Degree deg) : base(n, ssn, salry)
        {
            Degree = deg;
        }

        public override string MyType()
        {
            return "Teacher";
        }

        public override string ToString()
        {
            return String.Format("{5,10} [ID:{0,5}, Name:{1,-25}, SSN:{2,6}, Salary:{3,6}, Degree:{4,5}]"
                , ID, Name, SSN, Salary, Degree, "Teacher");
        }
    }

    class Employee : Worker
    {
        public Title JobTitle { get; set; }
        public Employee(string n, string ssn, double salary, Title title) : base(n, ssn, salary)
        {
            JobTitle = title;
        }

        public override string MyType()
        {
            return "Employee";
        }

        public override string ToString()
        {
            return String.Format("{5,10} [ID:{0,5}, Name:{1,-25}, SSN:{2,6}, Salary:{3,6}, Job:{4,5}]"
                , ID, Name, SSN, Salary, JobTitle, "Employee");
        }
    }
    class PloymorphisimDemo
    {

        public static Random rand = new Random();
        static void Main(string[] args)
        {

            Person[] gathering = new Person[100];

            string name;

            for (int i = 0; i < gathering.Length; i++)
            {
                switch(rand.Next(3))
                {
                    case 0:
                        gathering[i] = new Student(GenerateRandomName(), (1000 + i).ToString(), 1000 + i, (Major) rand.Next(6));
                        break;
                    case 1:
                        gathering[i] = new Teacher(GenerateRandomName(), (1000 + i).ToString(),  rand.Next(500,5000),(Degree)rand.Next(5));
                        break;
                    case 2:
                        gathering[i] = new Employee(GenerateRandomName(), (1000 + i).ToString(), rand.Next(500, 5000), (Title)rand.Next(4));
                        break;
                }

                Console.WriteLine(gathering[i].MyType());
               // Console.WriteLine(gathering[i].ToString());

            }

            Console.ReadKey();
        }


        public static string GenerateRandomName()
        {

            string[] ArabicNames = 
                { "Abbad", "Abbas", "Abd al-Aziz", "Abd al-Rahman", "Abd al-Uzza", "Abd Manaf",
                "Abd Rabbo", "Abdel Fattah", "Abdel Nour", "Abdi", "Abdolreza", "Abdu", "Abdul",
                "Abdul Ahad", "Abdul Ali", "Abdul Alim", "Abdul Azim", "Abdul Baqi", "Abdul Bari",
                "Abdul Basir", "Abdul Basit", "Abdul Ghaffar", "Abdul Ghani", "Abdul Hadi", "Abdul Hafiz", "Abdul Hai", "Abdul Hakim", "Abdul Halim", "Abdul Hamid", "Abdul Haq", "Abdul Hussein", "Abdul Jabbar", "Abdul Jalil", "Abdul Jamil", "Abdul Karim", "Abdul Khaliq", "Abdul Latif", "Abdul Majid", "Abdul Malik", "Abdul Mannan", "Abdul Monem", "Abdul Muttalib", "Abdul Qadir", "Abdul Qayyum", "Abdul Quddus", "Abdul Rashid", "Abdul Samad", "Abdul Sattar", "Abdul Wadud", "Abdul Wahhab", "Abdul Wahid", "Abdul Zahir", "Abdul Zahra", "Abdullah", "Abdur Rab", "Abdur Rahim", "Abdur Raqib", "Abdur Rauf", "Abdur Razzaq", "Abdus Sabur", "Abdus Salam", "Abdus Shakur", "Abid", "Abidin", "Abo", "Abu", "Abu Abdullah", "Abu al-Qasim", "Abu Bakr", "Abu Hafs", "Abu Hamza", "Abu Nasir", "Abu Nasr", "Abu'l-Fadl", "Adan", "Adeel", "Adeem", "Adem", "Aden", "Adham", "Adib", "Adil", "Adir", "Adli", "Adnan", "Afif", "Ahad", "Ahmad", "Ahmed Tijani", "Ahsan", "Akeem", "Akif", "Akram", "Alaa", "Aladdin", "Ali", "Ali Naqi", "Ali Reza", "Alim", "Aman", "Aman Ali", "Amanullah", "Amer", "Amin", "Amin al-Din", "Aminullah", "Amir", "Amjad", "Ammar", "Amr", "Anas", "Anis", "Anisur Rahman", "Anjem", "Anwar", "Anwaruddin", "Aqeel", "Ari", "Arif", "Asad", "Asadullah", "Asem", "Asghar", "Ashraf", "Asif", "Asil", "Aslam", "Ataullah", "Atif", "Atiq", "Atiqullah", "Awad", "Ayad", "Ayman/Aiman/Aimen/Aymen", "Ayub", "Azem", "Azhar", "Azim", "Azimullah", "Aziz", "Azizullah", "Azizur Rahman", "Azmi", "Badi", "Badr al-Din", "Bagher", "Baha", "Baha' al-Din", "Bahri", "Baki", "Bakir", "Bara", "Barkat Ali", "Barkatullah", "Bashar", "Bashir", "Basri", "Bilal", "Bilel", "Billah", "Boualem", "Boulos", "Boutros", "Brahim", "Burhan", "Burhan al-Din", "Caden", "Chadli", "Daniel", "Dastgir", "Daud", "Dawoud", "Dhikrullah", "Ehsanullah", "Ekram", "Fadel", "Fahd", "Faheem", "Fahmi", "Fahri", "Faisal", "Faiz", "Faizan", "Faizullah", "Fakhr al-Din", "Fakhraddin", "Fakhruddin", "Faqir", "Faraj", "Farhat", "Farid", "Fariduddin", "Faris", "Farooq", "Fasih", "Fathallah", "Fathi", "Fatin", "Fawaz", "Fawzi", "Fayez", "Fazel", "Fazl", "Fazl ur Rahman", "Fazlallah", "Fazli", "Fazlul Haq", "Fazlul Karim", "Fikri", "Fouad", "Fouzan", "Fuad", "Furkan", "Gaffar", "Gamil", "Ghanem", "Ghassan", "Ghiyath al-Din", "Ghulam", "Ghulam Faruq", "Ghulam Mohiuddin", "Gulzar", "Habib", "Habib ur Rahman", "Habibullah", "Hadem", "Hadi", "Hadid", "Hafeez", "Hafizullah", "Haitham", "Hajj", "Hajji", "Hakam", "Hakim", "Haldun", "Halim", "Hamdan", "Hamdi", "Hamid", "Hamid al-Din", "Hamidullah", "Hamza", "Hani", "Hanif", "Harbi", "Harun", "Hashem", "Hashim", "Hasib", "Hassan", "Hassim", "Hatem", "Hayatullah", "Haydar", "Hazem", "Hibat Allah", "Hichem", "Hidayatullah", "Hikmat", "Hilmi", "Hisham", "Hisham ud-Din", "Hossam", "Hurairah", "Husam ad-Din", "Hussein", "Ibrahim", "Ibro", "Idris", "Ihab", "Ihsan", "Ikhtiyar al-Din", "Ikramullah", "Ikrimah", "Ilyas", "Imad", "Imad al-Din", "Imran", "Imtiaz", "Inaam", "Inam-ul-Haq", "Inayatullah", "Iqbal", "Irfan", "Isa", "Ishak", "Ishtiaq", "Iskandar", "Ismail", "Ismat ad-Din", "Ismatullah", "Issam", "Izz al-Din", "Izzat", "Izzatullah", "Jabal", "Jaber", "Jabir", "Jabr", "Ja'far", "Jahid", "Jalal", "Jalal ad-Din", "Jamal", "Jamal ad-Din", "Jameel", "Jamil", "Jarrah", "Jasem", "Jawad", "Jawdat", "Jihad", "Jubayr", "Junayd", "Jurj", "Ka'b", "Kadeem", "Kadir", "Kadri", "Kafeel", "Kamal ad-Din", "Kamil", "Karem", "Karim", "Kashif", "Kazem", "Khadem", "Khair ad-Din", "Khalid", "Khalifah", "Khalil", "Khalil-ur-Rehman", "Khamis", "Kulthum", "Labib", "Lalji", "Latif", "Luay", "Lutfullah", "Lutfur Rahman", "Mahalati", "Mahbubur", "Mahdi", "Mahfuz", "Mahir", "Mahmud", "Majid", "Malik", "Mamdouh", "Mansur", "Manzur", "Marwan", "Mashallah", "Masoud", "Maytham", "Mehdi", "Melhem", "Michel", "Midhat", "Mizanur Rahman", "Moatassem", "Moeen", "Moemen", "Mohammad Taqi", "Mohannad", "Mohy al-Din", "Moin", "Moinuddin", "Mojtaba", "Moncef", "Moneim", "Mua'dh", "Muammer", "Mubarak", "Muhammad", "Muharrem", "Muhibullah", "Muhsin", "Mu'iz ad-Din", "Mukhtar", "Mumtaz", "Munib", "Munif", "Munir", "Murad", "Murtaza", "Musa", "Muslim", "Mustafa", "Muzaffar", "Nabih", "Nabil", "Nadeem", "Nader", "Nadir", "Nadur", "Naguib", "Nahyan", "Naif", "Naim", "Naji", "Najib", "Najibullah", "Najim", "Najm", "Najm al-Din", "Naqibullah", "Naseeb", "Naseer", "Nasim", "Nasir", "Nasir al-Din", "Nasrallah", "Nasri", "Nasser", "Nassif", "Nasuh", "Nawaf", "Nawaz", "Nawfal", "Nazif", "Nazim", "Nazimuddin", "Nazmi", "Nihad", "Nimatullah", "Nizam al-Din", "Nuh", "Nu'man", "Nur", "Nur al-Din", "Nuri", "Nurullah", "Nusrat", "Omar", "Osama", "Osman", "Qaid", "Qamar ud-Din", "Qasim", "Qasymbek", "Quddus", "Qudratullah", "Qusay", "Qutb", "Qutb ad-Din", "Rabih", "Raed", "Rafiq", "Rahim", "Rahman", "Rahmatullah", "Rahmi", "Rajab", "Rajaei", "Raji", "Ramiz", "Ramzan", "Ramzi", "Rashad", "Rashid", "Rashid al-Din", "Rasul", "Rayan", "Redouane", "Reza", "Riad", "Riaz", "Ridwan", "Rifat", "Rizqallah", "Ruhi", "Ruhullah", "Rukn al-Din", "Rushdi", "Sa‘id", "Saad", "Saadallah", "Sabah ad-Din", "Sabri", "Sa'd al-Din", "Saddam", "Sadik", "Sadr al-Din", "Safi", "Safi al-Din", "Safiullah", "Sahir", "Saif", "Saifullah", "Saifur Rahman", "Sajid", "Sajjad", "Salah", "Salah ad-Din", "Saleh", "Salem", "Salim", "Salman", "Samad", "Samee", "Samer", "Sami", "Samir", "Samirah", "Samiullah", "Sanaullah", "Saqib", "Sardar", "Sarmad", "Satam", "Sattar", "Sayf al-Din", "Sayyid", "Seif ilislam", "Shaban", "Shad", "Shafiq ur Rahman", "Shafiqullah", "shaheed", "Shahid", "Shakeel", "Shakir", "Shams", "Shams al-Din", "Shamsur Rahman", "Sharaf al-Din", "Sharifullah", "Shawkat", "Shawki", "Shihab al-Din", "Shujauddin", "Shukri", "Sidique", "Sidqi", "Sirajuddin", "Suhail", "Suleiman", "Sultan", "Taha", "Taher", "Tahmid", "Tahsin", "Talal", "Talat", "Talhah", "Talib", "Taqi", "Taqi al-Din", "Tarazi", "Tariq", "Tawfik", "Tayeb", "Tayfur", "Tufail", "Turki", "Ubay", "Ubayd Allah", "Uday", "Usama", "Uthman", "Wadih", "Wadud", "Wael", "Wafi", "Wahed", "Wahid", "Wajdi", "Wajid", "Waleed", "Waliullah", "Wasim", "Wazir", "Wise", "Wissem", "Yacinew", "Yadollah", "Yahir", "Yahya", "Yakub", "Yasser", "Yunus", "Yusha ", "Yusuf", "Yusuf Ali", "Zafar", "Zafarullah", "Zafer", "Zahed", "Zahir", "Zahir al-Din", "Zaid", "Zaim", "Zainal", "Zainal Abidin", "Zakariya", "Zaki", "Zane", "Zayn ad-Din", "Zeeshan", "Ziad", "Ziauddin", "Ziaur Rahman", "Zubayr" };

            //int n = rand.Next(3, 10);
            //StringBuilder build = new StringBuilder();
            //build.Append(Convert.ToChar(rand.Next('A', 'Z')));
            //for (int i = 1; i < n; i++)
            //{
            //    build.Append(Convert.ToChar(rand.Next('a', 'z')));
            //}

            //return build.ToString();

            //Thread.Sleep(1);

            return ArabicNames[rand.Next(ArabicNames.Length)];
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asp032019.oop
{
    class ThisAndBase
    {
        private int var;

        public void SetVar(int v)
        {
            // this : is  a refrence from insdie the object refrece to the object itself 
            this.var = v;
        }

        public override string ToString()
        {
            // base : is  a refrence from insdie the object refrece to the parent object. 
            return String.Format("Variable:{0} -",var)+ base.ToString();
        }
    }

   class Program
    {
        static void Main(string[] args)
        {
            ThisAndBase obj = new ThisAndBase();
            obj.SetVar(10);

            Console.WriteLine(obj);
            Console.ReadKey();
        }
    }
}

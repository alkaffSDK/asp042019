﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asp032019.oop
{
    class Human
    {
        public int  id;
        public override string ToString()
        {
            // return base.ToString();
            return "This is a Human with ID:"+id;
        }
    }
    class ObjectClass
    {
        static void Main(string[] args)
        {
            Human h = new Human();
            h.id = 5;
            Console.WriteLine(h);

            h.id = 8;
            Console.WriteLine(h.ToString());

            Console.ReadKey();
        
        }
    }
}

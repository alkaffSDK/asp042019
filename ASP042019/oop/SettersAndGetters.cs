﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asp032019.oop { 
     class User
    {
        public string Name;
        private int Age;
        
        public string Email;
        public string Password;

        public bool Login(string email, string pass)
        {
            return Email.Equals(email, StringComparison.OrdinalIgnoreCase) && Password.Equals(pass);
        }

        public override string ToString()
        {
            return string.Format("User [\'{0}\',{1},\'{2}\']", Name, Age, Email);
        }

        public void SetAge(int a)
        {
            if (a > 0 && a < 150)
                Age = a;
        }

        public int GetAge() { return Age; }

        public void SetName(string n)
        {
            if (!string.IsNullOrEmpty(n) && n.Length >= 3)
                Name = n;

        }
        public string GetName() { return Name; }

        public void SetEmail(string e)
        {
            int dotIndex = e.LastIndexOf(".");
            int atIndex = e.LastIndexOf("@");

            if (!string.IsNullOrEmpty(e) && e.Length >= 7 && dotIndex > 0 && atIndex > 0 && dotIndex > atIndex)
                Email  = e;

        }
        public string GetEmail() { return Email; }


        public bool ChangePassword(string pass)
        {
            if (!string.IsNullOrEmpty(pass) && pass.Length >= 6)
            {
                Password = pass;
                return true;
            }
            return false;
        }

        public bool CheckPassword(string pass )
        {
            return pass.Equals(Password);
        }
    }

    class SettersAndGetters
    {

        static void Main(string[] args)
        {
            User u = new User();
            u.Name = "Ahmed";
            u.SetAge(20);
            u.SetAge(-36);
            // u.Age = -36;
            //u.SetAge(36);
            u.Email = "a.alkaff@sdkjordan.com";
            Console.WriteLine("Your age :"+u.GetAge());
            Console.WriteLine(u);
            Console.WriteLine(u.ToString());
            Console.ReadKey();
        }
    }
}
